package mk.plugin.customskills.skill;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.customskills.skill.executor.AkaiSekaiExecutor;
import mk.plugin.customskills.skill.executor.AoiroSekaiExecutor;
import mk.plugin.customskills.skill.executor.HyokoExecutor;
import mk.plugin.customskills.skill.executor.IttouShuraExecutor;
import mk.plugin.customskills.skill.executor.KosokuExecutor;
import mk.plugin.customskills.skill.executor.RaikiriExecutor;
import mk.plugin.customskills.skill.executor.RyujinHyoiExecutor;
import mk.plugin.customskills.skill.executor.TaishinExecutor;
import mk.plugin.customskills.util.ItemBuilder;
import mk.plugin.customskills.util.Utils;

public enum Skill {

	RAIKIRI("Raikiri", new RaikiriExecutor()),
	AOIROSEKAI("Aoiro Sekai", new AoiroSekaiExecutor()),
	AKAISEKAI("Akai Sekai", new AkaiSekaiExecutor()),
	ITTOUSHURA("Ittou Shura", new IttouShuraExecutor()),
	TAISHIN("Taishin", new TaishinExecutor()),
	RYUJINHYOI("Ryoujin Hyoi", new RyujinHyoiExecutor()),
	KOSOKU("Kosoku", new KosokuExecutor()),
	HYOKO("Hyoko", new HyokoExecutor());
	
	private String name;
	private int cooldown;
	private SkillExecutor executor;
	private ItemStack icon;

	private Map<String, Double> values;
	
	private Skill(String name, SkillExecutor executor) {
		this.name = name;
		this.cooldown = 0;
		this.executor = executor;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getCooldown() {
		return this.cooldown;
	}
	
	public ItemStack getIcon() {
		return this.icon.clone();
	}
	
	public void setValues(Map<String, Double> values) {
		this.cooldown = values.get("cooldown").intValue();
		this.values = values;
	}
	
	public void execute(Player player, Map<String, Object> bonusValues, boolean ignoreCooldown) {
		// Check permission
		if (!SkillUtils.hasPermission(player, this)) {
			player.sendMessage("§cBạn không đủ quyền dùng kỹ năng này");
			return;
		}
		
		// Check condition
		if (!Utils.canCastSkillHere(player)) {
			player.sendMessage("§cBạn không thể dùng kỹ năng ở đây");
			return;
		}
		
		// Check cooldown
//		if (!ignoreCooldown) {
//			if (SkillUtils.isCooldown(player, this)) {
//				player.sendMessage("§eĐợi " + SkillUtils.getTimeRemain(player, this) + "s để thực thi §6" + this.getName());
//				return;
//			}
//		}
//		SkillUtils.cooldown(player, this);
		
		// Execute
		Map<String, Object> map = Maps.newHashMap();
		map.putAll(bonusValues);
		map.putAll(this.values);
		this.executor.run(player, map);
		
		// Title
		player.sendTitle("§c§l" + this.getName(), "§aThực thi kỹ năng", 0, 20, 0);
	}
	
	public Map<String, Double> getValues() {
		return this.values;
	}
	
	public void load(FileConfiguration config) {
		// Values
		String path = "skill." + this.name().toLowerCase();
		Map<String, Double> map = Maps.newHashMap();
		config.getConfigurationSection(path).getKeys(false).forEach(key -> {
			map.put(key, config.getDouble(path + "." + key));
		});
		this.setValues(map);
		
		// Icon
		if (config.contains("gui.icon." + this.name().toLowerCase())) {
			this.icon = ItemBuilder.buildItem(config.getConfigurationSection("gui.icon." + this.name().toLowerCase()));
		} else this.icon = new ItemStack(Material.STONE);
	}
	
	public static void loadAll(FileConfiguration config) {
		for (Skill skill : values()) skill.load(config);
	}
	
}
