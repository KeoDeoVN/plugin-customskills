package mk.plugin.customskills.skill;

import java.util.Map;

import org.bukkit.entity.Player;

public interface SkillExecutor {
	
	public void run(Player player, Map<String, Object> map);
	
}
