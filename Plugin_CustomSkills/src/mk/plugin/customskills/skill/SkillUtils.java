package mk.plugin.customskills.skill;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

public class SkillUtils {
	
	private static Map<Player, Map<Skill, Long>> cooldown = new  HashMap<Player, Map<Skill, Long>> ();
	
	public static int getTimeRemain(Player player, Skill skill) {
		if (isCooldown(player, skill)) {
			return skill.getCooldown() - new Double((System.currentTimeMillis() - cooldown.get(player).get(skill)) / 1000  * 100).intValue() / 100;
		}
		return 0;
	}
	
	public static boolean isCooldown(Player player, Skill skill) {
		if (cooldown.containsKey(player)) {
			Map<Skill, Long> map = cooldown.get(player);
			if (map.containsKey(skill)) {
				long time = map.get(skill);
				if (System.currentTimeMillis() - time >= skill.getCooldown() * 1000) {
					map.remove(skill);
					cooldown.put(player, map);
					return false;
				}
				else return true;
			}
		}
		
		return false;
	}
	
	public static void cooldown(Player player, Skill skill) {
		Map<Skill, Long> map = new HashMap<Skill, Long>  ();
		if (cooldown.containsKey(player)) {
			map = cooldown.get(player);
		}
		map.put(skill, System.currentTimeMillis());
		cooldown.put(player, map);
	}
	
	public static boolean hasPermission(Player player, Skill skill) {
		return player.hasPermission("customskills.skill." + skill.name().toLowerCase());
	}
	
	public static boolean hasPermission(Player player, String skill) {
		return player.hasPermission("customskills.skill." + skill.toLowerCase());
	}
	
	public static List<Skill> getActiveSkills(Player player) {
		List<Skill> list = Lists.newArrayList();
		for (Skill skill : Skill.values()) {
			if (hasPermission(player, skill)) list.add(skill);
		}
		return list;
	}
	
}
