package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mk.plugin.customskills.player.PlayerTimings;
import mk.plugin.customskills.skill.Skill;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.skill.SkillUtils;

public class HyokoExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		// TODO Auto-generated method stub
		
	}
	
	public static void execute(Player player) {
		if (!SkillUtils.hasPermission(player, "Hyoko")) return;
		
		double healthUp = Skill.HYOKO.getValues().get("health-up");
		
		boolean has = false;
		for (String key : PlayerTimings.getKeys(player)) {
			if (key.contains("Hyoko-")) {
				// Get and check
				int time = Math.min(50, Integer.valueOf(key.replace("Hyoko-", "")));
				
				// Set data
				time++;
				PlayerTimings.remove(player, key);
				PlayerTimings.add(player, "Hyoko-" + time, 3000);
				
				// Set
				has = true;
			}
		}
		
		if (!has) {
			PlayerTimings.add(player, "Hyoko-0", 3000);
		}
		
		player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60, 0), true);
		player.setHealth(Math.min(player.getHealth() + healthUp, player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()));
	}


}
