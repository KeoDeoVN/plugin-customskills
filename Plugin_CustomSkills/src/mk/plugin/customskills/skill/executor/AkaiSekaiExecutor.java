package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.util.Utils;

public class AkaiSekaiExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		double damage = (Double) map.get("damage");
		double height = (Double) map.get("fall-height");
		int potionLevel = ((Double) map.get("effect-level")).intValue();
		int fireBallAmount = ((Double) map.get("fireball-amount")).intValue();
		int waveAmount = ((Double) map.get("wave-amount")).intValue();
		int waveInterval = new Double(((Double) map.get("wave-interval")) * 20).intValue();
		
		new BukkitRunnable() {
			int c = 0;
			@Override
			public void run() {
				c++;
				if (c > waveAmount) {
					this.cancel();
					return;
				}
				
				// Fireballs
				Location location = player.getLocation().clone().add(0, height, 0);
				for (int i = 0 ; i < fireBallAmount ; i++) {
					// Random
					double x = player.getLocation().getX() + Utils.random(-5, 5);
					double z = player.getLocation().getZ() + Utils.random(-5, 5);
					Location temp = new Location(location.getWorld(), x, location.getY(), z);
					
					// Spawn
					Fireball fb = (Fireball) location.getWorld().spawnEntity(temp, EntityType.FIREBALL);
					fb.setMetadata("customskills-akaisekai", new FixedMetadataValue(MainCustomSkills.get(), damage));
					fb.setMetadata("customskills-akaisekai-player", new FixedMetadataValue(MainCustomSkills.get(), player.getName()));
					fb.setDirection(new Vector(0, -0.000000000000001, 0));
				}
			}
		}.runTaskTimer(MainCustomSkills.get(), 0, waveInterval);
		
		// Buff player
		player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, potionLevel));

	}

}
