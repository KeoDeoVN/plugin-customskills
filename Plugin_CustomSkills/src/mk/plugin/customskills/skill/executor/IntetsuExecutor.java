package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.entity.Player;

import mk.plugin.customskills.player.PlayerTimings;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.skill.SkillUtils;

public class IntetsuExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		
	}
	
	public static void damageOrGetDamaged(Player player) {
		if (!SkillUtils.hasPermission(player, "Intetsu")) return;
		boolean has = false;
		for (String key : PlayerTimings.getKeys(player)) {
			if (key.contains("Intetsu-")) {
				// Get and check
				int time = Math.min(100, Integer.valueOf(key.replace("Intetsu-", "")));
				
				// Set data
				time++;
				PlayerTimings.remove(player, key);
				PlayerTimings.add(player, "Intetsu-" + time, 2000);
			}
		}
		if (!has) PlayerTimings.add(player, "Intetsu-0", 2000);
	}

}
