package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.player.PlayerTimings;
import mk.plugin.customskills.skill.Skill;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.skill.SkillUtils;

public class KosokuExecutor implements SkillExecutor {

	public static Map<Player, Double> defaultMoveSpeeds = Maps.newHashMap();
	public static Map<Player, Double> defaultAttackSpeeds = Maps.newHashMap();
	
	@Override
	public void run(Player player, Map<String, Object> map) {
		// TODO Auto-generated method stub
		
	}
	
	public static void execute(Player player) {
		if (!SkillUtils.hasPermission(player, "Kosoku")) return;
		
//		double moveUp = (Double) Skill.KOSOKU.getValues().get("movespeed-up");
		double attackUp = (Double) Skill.KOSOKU.getValues().get("attackspeed-up");
		
		boolean has = false;
		int level = 0;
		for (String key : PlayerTimings.getKeys(player)) {
			if (key.contains("Kosoku-")) {
				// Get and check
				int time = Math.min(50, Integer.valueOf(key.replace("Kosoku-", "")));
				
				// Set data
				time++;
				level = time;
				PlayerTimings.remove(player, key);
				PlayerTimings.add(player, "Kosoku-" + time, 2000);
				
				// Set
				has = true;
			}
		}
		// Default value
//		double defaultMoveSpeed = defaultMoveSpeeds.get(player);
		double defaultAttackSpeed = defaultAttackSpeeds.get(player);
		
		if (!has) {
			PlayerTimings.add(player, "Kosoku-0", 2000);
		}
		
		// Set
		player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(defaultAttackSpeed + attackUp * level);
//		player.setWalkSpeed(Math.min(new Double(defaultMoveSpeed + moveUp * level).floatValue(), 1));
		
		// Task check
//		double dMS = defaultMoveSpeed;
		double dAS = defaultAttackSpeed;
		Bukkit.getScheduler().runTaskLater(MainCustomSkills.get(), () -> {
			boolean isRemained = false;
			for (String key : PlayerTimings.getKeys(player)) {
				if (key.contains("Kosoku-")) {
					isRemained = true;
					break;
				}
			}
			if (!isRemained) {
				player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(dAS);
//				player.setWalkSpeed(new Double(dMS).floatValue());
			}
		}, 20 * 3);
	}

}
