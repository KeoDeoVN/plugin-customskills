package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.util.Utils;

public class TaishinExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		double damage = (Double) map.get("damage");
		double radius = (Double) map.get("radius");
		
		// Effect
		player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60, 1));
		
		new BukkitRunnable() {			
			int c = 0;			
			@Override
			public void run() {
				// Check time
				c++;
				if (c > 3) {
					this.cancel();
					return;
				}
				
				// Particle
				double r = 0.4;
				for (int i = 1 ; i * r < radius ; i ++) {
					Utils.circleParticles(Particle.CRIT, player.getLocation().add(0, 0.2, 0), i * r);
					Utils.circleParticles(Particle.FALLING_DUST, player.getLocation().add(0, 0.2, 0), i * r);
				}
				
				// Sound
				player.playSound(player.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
				
				// Entity
				Bukkit.getScheduler().runTask(MainCustomSkills.get(), () -> {
					Utils.getLivingEntities(player, player.getLocation(), radius, 1, radius).forEach(le -> {
						Utils.damage(player, le, damage, 0);
						le.setVelocity(new Vector(0, 0.5, 0));
					});
				});

			}
		}.runTaskTimerAsynchronously(MainCustomSkills.get(), 0, 20);
	}

}
