package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;

import mk.plugin.customskills.player.PlayerTimings;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.skill.SkillUtils;
import mk.plugin.customskills.util.Utils;

@SuppressWarnings("deprecation")
public class YoishigureExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		// TODO Auto-generated method stub
	}
	
	public static void execute(EntityDamageByEntityEvent e, Player player, Entity entity) {
		if (!SkillUtils.hasPermission(player, "Yoishigure")) return;
		boolean has = false;
		for (String key : PlayerTimings.getKeys(player)) {
			if (key.contains("Yoishigure-")) {
				// Get and check
				int time = Math.min(50, Integer.valueOf(key.replace("Yoishigure-", "")));
				
				// Set data
				time++;
				PlayerTimings.remove(player, key);
				PlayerTimings.add(player, "Yoishigure-" + time, 3000);
				
				// Rate 25%
				if (!Utils.rate(25)) return;
				
				// Particle
				player.getWorld().spawnParticle(Particle.WATER_DROP, entity.getLocation().add(0, 1, 0), 25, 0.5f, 0.5f, 0.5f, 0.1f);
				player.playSound(entity.getLocation(), Sound.ENTITY_GENERIC_SWIM, 1, 1);
				
				// True damage chance 
				double tdc = time * 0.3;
				if (Utils.rate(tdc)) {
					try {
						e.setDamage(DamageModifier.ARMOR, 0);
						player.sendMessage("§6[Yoishigure] §eXuyên giáp");
					}
					catch (UnsupportedOperationException ex) {
						has = true;
					}
				}
				
				// Set
				has = true;
			}
		}
		if (!has) PlayerTimings.add(player, "Yoishigure-0", 3000);
	}

}
