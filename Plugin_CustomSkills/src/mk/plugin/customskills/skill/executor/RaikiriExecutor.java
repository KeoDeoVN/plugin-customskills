package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.util.Utils;

public class RaikiriExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		double velocityMultiple = (Double) map.get("velocity-multiple");
		double damage = (Double) map.get("damage");
		int strikeAmount = new Double((Double) map.get("strike-amount")).intValue();
		
		// Fly follow direction
		player.setVelocity(player.getLocation().getDirection().multiply(velocityMultiple));
		Utils.collisionDamage(player, damage, 500, 1000);	
		
		// Lighting strike after 2s
		Bukkit.getScheduler().runTaskLater(MainCustomSkills.get(), () -> {
			for (int i = 0 ; i < strikeAmount ; i++) {
				// Random location
				double x = player.getLocation().getX() + Utils.random(-5, 5);
				double z = player.getLocation().getZ() + Utils.random(-5, 5);
				Location temp = new Location(player.getLocation().getWorld(), x, player.getLocation().getY(), z);
				
				// Get safe location
				int j = 0;
				while (temp.getBlock().getType() == Material.AIR) {
					j++;
					if (j > 10) {
						return;
					}
					temp = temp.add(0,-1,0);
				}
				temp = temp.add(0, 1, 0);
				
				// Strike
				player.getWorld().strikeLightningEffect(temp);
				Utils.damageEntitiesNearby(player, temp, 3, 3, 3, damage, 100);
				
				// Effect
				player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, temp.clone().add(0, 0.7, 0), 1, 0, 0, 0, 0);
			}
		}, 40);
		
	}

}
