package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import mk.plugin.customskills.player.PlayerTimings;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.skill.SkillUtils;

public class RyujinHyoiExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		
	}
	
	public static void execute(EntityDamageByEntityEvent e, Player player) {
		if (!SkillUtils.hasPermission(player, "RyujinHyoi")) return;
		boolean has = false;
		for (String key : PlayerTimings.getKeys(player)) {
			if (key.contains("RyujinHyoi-")) {
				// Get and check
				int time = Math.min(200, Integer.valueOf(key.replace("RyujinHyoi-", "")));
				
				// Set data
				time++;
				PlayerTimings.remove(player, key);
				PlayerTimings.add(player, "RyujinHyoi-" + time, 3000);
				
				// Set damage
				e.setDamage(e.getDamage() + 0.5 * time);
				
				// Set
				has = true;
			}
		}
		if (!has) {
			PlayerTimings.add(player, "RyujinHyoi-0", 3000);
		}
	}

}
