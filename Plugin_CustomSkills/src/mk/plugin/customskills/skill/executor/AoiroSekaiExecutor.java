package mk.plugin.customskills.skill.executor;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.google.common.collect.Maps;

import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.skill.SkillExecutor;
import mk.plugin.customskills.util.Utils;

public class AoiroSekaiExecutor implements SkillExecutor {

	@Override
	public void run(Player player, Map<String, Object> map) {
		double damage = (Double) map.get("damage");
		double range = (Double) map.get("range");
		double velocityMultiple = (Double) map.get("velocity-multiple");
		
		// Check target
		LivingEntity target = Utils.getTarget(player, range);
		if (target == null) {
			player.sendMessage("§cKhông tìm thấy mục tiêu");
			return;
		}
		
		// Default block
		Map<Location, Material> defaultBlocks = Maps.newHashMap();
		
		// Do target
		Location tL = target.getLocation();
		
		Utils.getLivingEntities(player, tL, 7, 7, 7).forEach(le -> {
			Utils.damage(player, le, damage, 0);
			le.setVelocity(new Vector(0, 1, 0).multiply(velocityMultiple));
			player.playSound(target.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1, 1);
			le.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 60, 3));
			
			Location iL1 = le.getLocation().clone();
			Location iL2 = le.getLocation().clone().add(0, 1, 0);
			
			defaultBlocks.put(iL1, iL1.getBlock().getType());
			defaultBlocks.put(iL2, iL2.getBlock().getType());
		});
		
		// Change block
		
		// 0.7 per time to get all blocks
		for (int i = 0 ; i < 10 ; i++) {
			double r = i * 0.7;
			Utils.circle(tL.clone().add(0, -1, 0), r).forEach(l -> {
				defaultBlocks.put(l, l.getBlock().getType());
			});
		}
		
		// Change blocks
		defaultBlocks.keySet().forEach(l -> l.getBlock().setType(Material.PACKED_ICE));
		
		// Rechange blocks
		Bukkit.getScheduler().runTaskLater(MainCustomSkills.get(), () -> {
			defaultBlocks.forEach((l, t) -> {
				if (l.getBlock().getType() != t) l.getBlock().setType(t);
			});
		}, 3 * 20);
		
		
	}

}
