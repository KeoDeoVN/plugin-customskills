package mk.plugin.customskills.player;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class PlayerTimings {
	
	private static Map<String, Map<String, Long>> data = Maps.newHashMap();
	
	public static List<String> getKeys(Player player) {
		List<String> keys = Lists.newArrayList();
		if (!data.containsKey(player.getName())) return keys;
		
		Map<String, Long> pData = data.get(player.getName());
		Sets.newHashSet(pData.keySet()).forEach(key -> {
			if (has(player, key)) keys.add(key);
		});
		
		return keys;
	}
	
	public static boolean has(Player player, String key) {
		if (!data.containsKey(player.getName())) return false;
		Map<String, Long> pData = data.get(player.getName());
		
		if (!pData.containsKey(key)) return false;
		if (System.currentTimeMillis() > pData.get(key)) {
			pData.remove(key);
			data.put(player.getName(), pData);
			return false;
		}
		return true;
	}
	
	public static void add(Player player, String key, long timeExist) {
		Map<String, Long> pData = Maps.newHashMap();
		if (data.containsKey(player.getName())) pData = data.get(player.getName());
		pData.put(key, System.currentTimeMillis() + timeExist);
		data.put(player.getName(), pData);
	}
	
	public static void remove(Player player, String key) {
		Map<String, Long> pData = Maps.newHashMap();
		if (data.containsKey(player.getName())) pData = data.get(player.getName());
		pData.remove(key);
		data.put(player.getName(), pData);
	}
	
	public static String find(Player player, String contain) {
		for (String key : PlayerTimings.getKeys(player)) {
			if (key.contains(contain)) return key;
		}
		return null;
	}
	
}
