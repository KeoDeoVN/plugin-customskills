package mk.plugin.customskills.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.customskills.command.AdminCommand;
import mk.plugin.customskills.command.PlayerCommand;
import mk.plugin.customskills.config.Configs;
import mk.plugin.customskills.listener.ComboListener;
import mk.plugin.customskills.listener.SkillListener;
import mk.plugin.customskills.skill.Skill;
import mk.plugin.customskills.yaml.YamlFile;

public class MainCustomSkills extends JavaPlugin {

	@Override
	public void onEnable() {
		this.reloadConfigs();
		this.registerLiseners();
		this.registerCommands();
	}
	
	public void reloadConfigs() {
		YamlFile.reloadAll(this);
		Configs.reload(YamlFile.CONFIG.get());
		Skill.loadAll(YamlFile.CONFIG.get());
	}
	
	public void registerLiseners() {
		Bukkit.getPluginManager().registerEvents(new ComboListener(), this);
		Bukkit.getPluginManager().registerEvents(new SkillListener(), this);
	}
	
	public void registerCommands() {
		this.getCommand("customskills").setExecutor(new AdminCommand());
		this.getCommand("skilllist").setExecutor(new PlayerCommand());
	}
	
	public static MainCustomSkills get() {
		return MainCustomSkills.getPlugin(MainCustomSkills.class);
	}
	
}
