package mk.plugin.customskills.gui;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import mk.plugin.customskills.config.Configs;
import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.skill.Skill;
import mk.plugin.customskills.skill.SkillUtils;

public class SkillGUI {
	
	public static void open(Player player) {
		// Get
		List<Skill> skills = SkillUtils.getActiveSkills(player);
		int size = skills.size() % 9 == 0 ? skills.size() : (skills.size() / 9 + 1) * 9;
		Inventory inv = Bukkit.createInventory(new SkillGUIHolder(), size, Configs.GUI_TITLE);
		player.openInventory(inv);
		
		// Load
		Bukkit.getScheduler().runTaskLaterAsynchronously(MainCustomSkills.get(), () -> {
			for (int i = 0 ; i < skills.size() ; i++) {
				inv.setItem(i, skills.get(i).getIcon());
			}
		}, 1);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		if (inv.getHolder() instanceof SkillGUIHolder) e.setCancelled(true);
	}
	
	public static void eventDrag(InventoryDragEvent e) {
		Inventory inv = e.getInventory();
		if (inv.getHolder() instanceof SkillGUIHolder) e.setCancelled(true);
	}
	
}

class SkillGUIHolder implements InventoryHolder {

	@Override
	public Inventory getInventory() {
		// TODO Auto-generated method stub
		return null;
	}

}
