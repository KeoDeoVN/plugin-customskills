package mk.plugin.customskills.combo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.customskills.config.Configs;
import mk.plugin.customskills.util.Utils;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class ComboUtils {
	
	private static Map<Player, Map<Combo, Long>> cooldown = Maps.newHashMap();
	private static Map<String, Combo> playerActiveCombos = Maps.newHashMap();
	
	public static int getTimeRemain(Player player, Combo combo) {
		if (isCooldown(player, combo)) {
			for (Combo cb : cooldown.get(player).keySet()) {
				if (cb.equals(combo)) {
					return new Double(cooldown.get(player).get(cb) - System.currentTimeMillis()).intValue() / 1000;
				}
			}
		}
		return 0;
	}
	
	public static boolean isCooldown(Player player, Combo combo) {
		if (cooldown.containsKey(player)) {
			Map<Combo, Long> map = cooldown.get(player);
			for (Combo cb : map.keySet()) {
				long time = map.get(cb);
				if (cb.equals(combo)) {
					if (System.currentTimeMillis() >= time) {
						map.remove(cb);
						cooldown.put(player, map);
						return false;
					}
					else return true;
				}
			}
		}
		
		return false;
	}
	
	public static void cooldown(Player player, Combo combo) {
		Map<Combo, Long> map = new HashMap<Combo, Long>  ();
		if (cooldown.containsKey(player)) {
			map = cooldown.get(player);
		}
		map.put(combo, System.currentTimeMillis() + combo.getCooldown() * 1000);
		cooldown.put(player, map);
	}
	
	public static void startCombo(Player player) {
		Combo cb = new Combo(Lists.newArrayList());
		playerActiveCombos.put(player.getName(), cb);
	}
	
	public static Combo getCombo(Player player) {
		return playerActiveCombos.getOrDefault(player.getName(), null);
	}
	
	public static void addClick(Player player, int index) {
		// Add
		Combo cb = getCombo(player);
		if (cb == null) {
			cb = new Combo(Lists.newArrayList());
			playerActiveCombos.put(player.getName(), cb);
		}
		cb.add(index);
		
		// Check commands
		List<String> cmds = matchCommands(player, cb);
		if (cmds == null) return;
		
		// Check cooldown
		if (isCooldown(player, cb)) {
			player.sendMessage("§eĐợi " + getTimeRemain(player, cb) + "s để thực thi kỹ năng");
			return;
		}
		cooldown(player, cb);
		
		// Run commands
		cmds.forEach(cmd -> {
			Utils.runCommand(player, cmd);
		});
		
		// Remove combo
		remove(player);
	}
	
	public static void remove(Player player) {
		playerActiveCombos.remove(player.getName());
		sendActionbar(player, "");
	}
	
	public static List<String> matchCommands(Player player, Combo cb) {
		for (Combo key : Configs.COMBO_COMMANDS.keySet()) {
			
			// Check permission
			if (!player.hasPermission(cb.getPermission())) continue;
			
			// Match
			if (key.equals(cb)) return Configs.COMBO_COMMANDS.get(key);
		}
		return null;
	}
	
	public static void showCombo(Player player, Combo cb) {
		if (cb == null) return;
		// Get message
		String message = "";
		for (int i : cb.getIndexes()) {
			ComboClick cc = ComboClick.fromIndex(i);
			message += "§a§l" +  cc.getName() + " §f§l+ ";	
		}
		message = message.substring(0, message.length() - " §f§l+ ".length());
		
		// Show
		sendActionbar(player, message);
	}
	
	public static void sendActionbar(Player player, String message) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(message));
	}
	
	
	
	
	
	
	
	
	
	
	
}
