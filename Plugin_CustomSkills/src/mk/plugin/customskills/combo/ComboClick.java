package mk.plugin.customskills.combo;

import org.bukkit.event.block.Action;

public enum ComboClick {
	
	LEFT("Trái", 0, Action.LEFT_CLICK_AIR),
	RIGHT("Phải", 1, Action.RIGHT_CLICK_AIR);
	
	private String name;
	private int index;
	private Action action;
	
	private ComboClick(String name, int index, Action action) {
		this.name = name;
		this.index = index;
		this.action = action;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public Action getAction() {
		return this.action;
	}
	
	public static ComboClick fromIndex(int index) {
		for (ComboClick cc : values()) {
			if (cc.getIndex() == index) return cc;
		}
		return null;
	}
	
	public static ComboClick fromAction(Action action) {
		for (ComboClick cc : values()) {
			if (cc.getAction() == action) return cc;
		}
		return null;
	}
	
}
