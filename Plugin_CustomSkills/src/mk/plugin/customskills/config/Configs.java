package mk.plugin.customskills.config;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.customskills.combo.Combo;

public class Configs {
	
	public static Map<Combo, List<String>> COMBO_COMMANDS = Maps.newHashMap();
	public static Map<Combo, String> COMBO_PERMISSIONS = Maps.newHashMap();
	public static Map<Combo, Integer> COMBO_DELAYS = Maps.newHashMap();
	public static String GUI_TITLE;
	
	public static void reload(FileConfiguration config) {
		COMBO_COMMANDS.clear();
		COMBO_PERMISSIONS.clear();
		COMBO_DELAYS.clear();
		config.getConfigurationSection("combo").getKeys(false).forEach(id -> {
			String path = "combo." + id;
			List<Integer> indexes = Lists.newArrayList(config.getString(path + ".clicks").split(";")).stream().map(Integer::valueOf).collect(Collectors.toList());
			Combo cb = new Combo(indexes);
			COMBO_COMMANDS.put(cb, config.getStringList(path + ".commands"));
			COMBO_PERMISSIONS.put(cb, config.getString(path + ".permission"));
			COMBO_DELAYS.put(cb, config.getInt(path + ".delay"));
		});
		
		GUI_TITLE = config.getString("gui.title").replace("&", "§");
	}
	
}
