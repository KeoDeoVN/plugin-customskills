package mk.plugin.customskills.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.customskills.gui.SkillGUI;

public class PlayerCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		if (sender instanceof Player == false) {
			sender.sendMessage("§cVào game mà command");
			return false;
		}
		
		if (cmd.getName().equalsIgnoreCase("skilllist")) {
			Player player = (Player) sender;
			SkillGUI.open(player);
		}
		
		return false;
	}

}
