package mk.plugin.customskills.command;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.customskills.main.MainCustomSkills;
import mk.plugin.customskills.skill.Skill;

public class AdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		try {
			
			if (args[0].equalsIgnoreCase("reload")) {
				MainCustomSkills.get().reloadConfigs();
				sender.sendMessage("§aReloaded");
			}
			
			else if (args[0].equalsIgnoreCase("skilllist")) {
				sender.sendMessage("");
				for (Skill skill : Skill.values()) {
					sender.sendMessage("§c§l" + skill.name());
				}
				sender.sendMessage("");
			}
			
			else if (args[0].equalsIgnoreCase("cast")) {
				// Get
				boolean ignoreCD = Boolean.valueOf(args[3]);
				Player player = Bukkit.getPlayer(args[1]);
				Skill skill = Skill.valueOf(args[2].toUpperCase());
			
				// Execute
				skill.execute(player, Maps.newHashMap(), ignoreCD);
			}
			
			else if (args[0].equalsIgnoreCase("resetstat")) {
				Player player = Bukkit.getPlayer(args[1]);
				player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16);
				player.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.2);
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendHelp(sender);
		}
		
		
		return false;
	}
	
	public void sendHelp(CommandSender sender) {
		sender.sendMessage("");
		sender.sendMessage("§6/customskills reload: §eReload config");
		sender.sendMessage("§6/customskills skilllist: §eXem danh sách kỹ năng");
		sender.sendMessage("§6/customskills resetstat <player>: §eReset chỉ số về mặc định minecraft (đề phòng lỗi)");
		sender.sendMessage("§6/customskills cast <player> <skill> <bỏ qua hồi chiêu(true|false): §eThực thi kỹ năng");
		sender.sendMessage("");
	}

}
