package mk.plugin.customskills.listener;

import java.util.stream.IntStream;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import mk.plugin.customskills.gui.SkillGUI;
import mk.plugin.customskills.player.PlayerTimings;
import mk.plugin.customskills.skill.executor.HyokoExecutor;
import mk.plugin.customskills.skill.executor.IntetsuExecutor;
import mk.plugin.customskills.skill.executor.IttouShuraExecutor;
import mk.plugin.customskills.skill.executor.KosokuExecutor;
import mk.plugin.customskills.skill.executor.RyujinHyoiExecutor;
import mk.plugin.customskills.skill.executor.YoishigureExecutor;
import mk.plugin.customskills.util.Utils;

public class SkillListener implements Listener {
	
	// Get default stats
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		KosokuExecutor.defaultAttackSpeeds.put(player, player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getValue());
		KosokuExecutor.defaultMoveSpeeds.put(player, player.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).getValue());
	}
	
	// Remove
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		KosokuExecutor.defaultAttackSpeeds.remove(player);
		KosokuExecutor.defaultMoveSpeeds.remove(player);
	}
	
	@EventHandler
	public void onGUIClick(InventoryClickEvent e) {
		SkillGUI.eventClick(e);
	}
	
	@EventHandler
	public void onGUIDrag(InventoryDragEvent e) {
		SkillGUI.eventDrag(e);
	}
	
	@EventHandler
	public void onFireballExplode(EntityExplodeEvent e) {
		if (e.getEntity() instanceof Fireball) {
			Fireball fb = (Fireball) e.getEntity();
			if (!fb.hasMetadata("customskills-akaisekai-player")) return;
			Player player = Bukkit.getPlayer(fb.getMetadata("customskills-akaisekai-player").get(0).asString());
			if (player == e.getEntity()) {
				e.setCancelled(true);
				return;
			}
			double damage = fb.getMetadata("customskills-akaisekai").get(0).asDouble();
			Utils.damageEntitiesNearby(player, fb.getLocation().add(0, -1, 0), 3, 3, 3, damage, 100);
			
		}
	}
	
	@EventHandler
	public void onPlayerDamaged(EntityDamageByEntityEvent e) {
		// Player damage
		if (e.getDamager() instanceof Player) {
			Player player = (Player) e.getDamager();
			
			// Ryujin Hyoi
			RyujinHyoiExecutor.execute(e, player);
			
			// Yoshigure
			YoishigureExecutor.execute(e, player, e.getEntity());
			
			// Intetsu
			IntetsuExecutor.damageOrGetDamaged(player);
			
			// Kosoku
			KosokuExecutor.execute(player);
		}
		
		// Player damaged
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			
			// Ittou Shura
			IttouShuraExecutor.execute(e, player);
			
			// Intetsu
			IntetsuExecutor.damageOrGetDamaged(player);
			
			// Hyoko
			HyokoExecutor.execute(player);
		}
	}
	
	
	@EventHandler
	public void onRegainHealth(EntityRegainHealthEvent e) {
		if (!(e.getEntity() instanceof Player)) return;
		Player player = (Player) e.getEntity();
		
		// Check Intetsu
		for (String key : PlayerTimings.getKeys(player)) {
			if (key.contains("Intetsu-")) {
				// Get and check
				int time = Math.min(50, Integer.valueOf(key.replace("Intetsu-", "")));
				
				// Buff
				IntStream.range(0, time).forEach(i -> e.setAmount(e.getAmount() * 1.1));
			}
		}
	}
}
